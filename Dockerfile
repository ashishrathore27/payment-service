FROM gradle:6.4.1-jdk11 as builder
WORKDIR /home/gradle/src
COPY --chown=gradle:gradle build.gradle.kts .
COPY --chown=gradle:gradle settings.gradle.kts .
COPY --chown=gradle:gradle src src
RUN gradle build --no-daemon
RUN cp build/libs/*.jar /home/gradle/src/app.jar
RUN java -Djarmode=layertools -jar app.jar extract


FROM mcr.microsoft.com/java/jre:11u7-zulu-alpine
RUN addgroup -S sparknow && adduser -S sparknow -G sparknow && mkdir -p app
USER sparknow:sparknow
WORKDIR app
COPY --from=builder /home/gradle/src/dependencies/ ./
COPY --from=builder /home/gradle/src/spring-boot-loader/ ./
COPY --from=builder /home/gradle/src/snapshot-dependencies/ ./
COPY --from=builder /home/gradle/src/application/ ./
ARG PORT=9010
ENV PORT=${PORT}
EXPOSE ${PORT}
ENTRYPOINT ["java","org.springframework.boot.loader.JarLauncher"]

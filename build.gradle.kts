import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.3.1.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.3.72"
    kotlin("plugin.spring") version "1.3.72"
    groovy apply true
}

group = "com.sparknow.platform"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    implementation(group = "com.stripe", name = "stripe-java", version = "19.26.0")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("com.google.firebase:firebase-admin:6.14.0")
    implementation (group = "com.fasterxml.jackson.module", name= "jackson-module-kotlin", version= "2.11.1")

    implementation(group = "org.aspectj", name = "aspectjweaver", version = "1.9.6")
    implementation(group = "net.logstash.logback", name = "logstash-logback-encoder", version = "6.4")


    testImplementation("org.codehaus.groovy:groovy-all:2.5.8")
    testImplementation("org.spockframework:spock-core:1.0-groovy-2.4")
    testImplementation("org.objenesis:objenesis:2.2")
    testImplementation("cglib:cglib-nodep:3.2.0")
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
    layered {
        isIncludeLayerTools = true
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
tasks.test {
    testLogging {
        showStandardStreams = true
        events("passed", "skipped", "failed", "standardOut", "standardError")
    }
}
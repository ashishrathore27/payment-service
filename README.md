
# Payment-Service 

##### Payment Service exposes Rest services that allows payment operations


### Build pre-requisites
- Kotlin: 1.3.2
- JDK: Java 11+
- Groovy SDK: 2.4.6+
- Firestore

### Technologies & frameworks
- Development: Kotlin, Spring Boot
- Testing: Groovy, Spock
- Build: Gradle

### Steps to build
- Clone the project
```
git clone git@gitlab.com:products/sparknow/payment-com.sparknow.platform.paymentservice.service.git
```
- Generate files for IntelliJ
```
gradlew cleanIdea idea
```
- Generate files for eclipse/STS
```
gradlew cleanEclipse eclipse
```
#### The Payment Service
- Build the project
```
gradlew clean build
```
- Code coverage report

- Start the Payment Service
```
gradlew bootRun
```
Alternatively, you can also use
```
gradlew bootRun
```


### Contact
nilesh.miskin@innovect.com
abhimanyu.singh@innovect.com
ashish.rathore@innovect.com
package com.sparknow.platform.paymentservice.util

import com.sparknow.platform.paymentservice.model.OrderDetails
import com.sparknow.platform.paymentservice.model.SettlementAccount
import com.sparknow.platform.paymentservice.util.paymentProcessor.stripe.PaymentIntentFactory
import spock.lang.Specification

class PaymentIntentFactoryTest extends Specification {

    PaymentIntentFactory paymentIntentFactory

    def setup() {
        paymentIntentFactory = new PaymentIntentFactory("TestKey")
    }

    def "calculateTransferAmount: Scenario 1"() {
        given:
        def platformFees = 2.15d

        def settlementAccount = new SettlementAccount("BID1", SettlementAccount.AccountType.STRIPE)
        settlementAccount.sparknowFee = 4.99
        settlementAccount.fixedFee = 35

        def orderDetails = new OrderDetails("Sample Order details", "Bid1", "MID1", "TestResource",  "USD", 1000,1200, "Online")

        when:
        def result = paymentIntentFactory.calculateTransferAmount(orderDetails, platformFees, settlementAccount)
        then:
        result == 1085
    }

    def "calculateTransferAmount: Scenario 2"() {
        given:
        def platformFees = 2.15d
        def settlementAccount = new SettlementAccount("BID1", SettlementAccount.AccountType.STRIPE)
        settlementAccount.sparknowFee = 4.99
        settlementAccount.fixedFee = 35

        def orderDetails = new OrderDetails("Sample Order details", "Bid1", "MID1", "TestResource",  "USD", 1000,1200, "Online")


        when:
        def result = paymentIntentFactory.calculateTransferAmount(orderDetails, platformFees, settlementAccount)

        then:
        result == 2204
    }

    def "calculateTransferAmount: Zero sparknow platform fee"() {
        given:
        def platformFees = 0d

        def settlementAccount = new SettlementAccount("BID1", SettlementAccount.AccountType.STRIPE)
        settlementAccount.sparknowFee = 4.99
        settlementAccount.fixedFee = 35

        def orderDetails = new OrderDetails("Sample Order details", "Bid1", "MID1", "TestResource",  "USD", 1000,1200, "Online")

        when:
        def result = PaymentIntentFactory.calculateTransferAmount(orderDetails, platformFees, settlementAccount)

        then:
        result == 2245
    }

}

package com.sparknow.platform.paymentservice.stub

import com.sparknow.platform.paymentservice.model.OrderDetails
import com.sparknow.platform.paymentservice.model.PaymentStatus
import com.sparknow.platform.paymentservice.model.SettlementAccount
import com.sparknow.platform.paymentservice.model.StripePaymentResponse
import com.stripe.model.Charge
import com.stripe.model.ChargeCollection
import com.stripe.model.PaymentIntent

class PaymentStub {

    static StripePaymentResponse getPaymentResponse() {
        def paymentResponse = new StripePaymentResponse(getPaymentIntent())
        paymentResponse.additionalActionRequired = false
        paymentResponse.message = "Payment complete"
        paymentResponse.status = PaymentStatus.SUCCESS
        return paymentResponse
    }

    static PaymentIntent getPaymentIntent() {
        PaymentIntent paymentIntent = new PaymentIntent()
        Charge charge = new Charge()
        charge.amount = 1700
        charge.balanceTransaction = "txn-123"
        ChargeCollection chargeCollection = new ChargeCollection()
        chargeCollection.setData(Collections.singletonList(charge))
        paymentIntent.setCharges(chargeCollection)
        paymentIntent.id = "PI-123-345"
        paymentIntent.clientSecret = "CI-1234-8765"
        paymentIntent.paymentMethod = "CARD"
        paymentIntent.status = "succeeded"
        return paymentIntent
    }

    static OrderDetails getOrderDetails() {
        OrderDetails orderDetails = new OrderDetails(
                "ord-123-456",
                "bi-123-456",
                "mi-123-457",
                "rsc-001",
                "USA",
                1000,
                1020,
                "DINE_IN"
        )
        return orderDetails
    }

    static SettlementAccount getSettlementAmount() {
        def accountId = "test-connected-id\""
        def platformFees = 9.9D
        SettlementAccount settlementAccount = new SettlementAccount("bi-123-456",SettlementAccount.AccountType.STRIPE)
        settlementAccount.id = "Id-001"
        settlementAccount.accountId = accountId
        settlementAccount.platformFees = platformFees
        settlementAccount.active = true
        settlementAccount.additionalInfo = new HashMap<String, String>()
        settlementAccount.sparknowFee = 2.15D
        return settlementAccount
    }
}

package com.sparknow.platform.paymentservice.service

import com.sparknow.platform.paymentservice.model.Business
import com.sparknow.platform.paymentservice.model.OrderDetails
import com.sparknow.platform.paymentservice.model.PaymentRecordFactory
import com.sparknow.platform.paymentservice.model.StripePaymentRequest
import com.sparknow.platform.paymentservice.model.PaymentStatus
import com.sparknow.platform.paymentservice.model.SettlementAccount
import com.sparknow.platform.paymentservice.repository.FirestoreRepository
import com.sparknow.platform.paymentservice.util.paymentProcessor.PaymentRecordRepository
import com.sparknow.platform.paymentservice.util.paymentProcessor.stripe.PaymentIntentFactory
import com.sparknow.platform.paymentservice.util.paymentProcessor.stripe.StripePaymentProcessor
import com.sparknow.platform.paymentservice.util.serviceAdapter.MerchantServiceAdapter
import com.stripe.model.Charge
import com.stripe.model.ChargeCollection
import com.stripe.model.PaymentIntent
import spock.lang.Specification

import static com.sparknow.platform.paymentservice.stub.PaymentStub.*

class StripePaymentProcessorTest extends Specification {
    StripePaymentProcessor stripePaymentProcessor
    FirestoreRepository mockFirestoreRepository
    MerchantServiceAdapter mockMerchantServiceAdapter
    PaymentRecordRepository paymentRecordRepository
    PaymentIntentFactory paymentIntentFactory
    PaymentRecordFactory paymentRecordFactory

    def setup() {
        paymentIntentFactory = Mock(PaymentIntentFactory)
        mockFirestoreRepository = Mock(FirestoreRepository)
        mockMerchantServiceAdapter = Mock(MerchantServiceAdapter)
        paymentRecordRepository = Mock(PaymentRecordRepository)
        paymentRecordFactory = Mock(PaymentRecordFactory)

        def map = ["Merchant" : mockMerchantServiceAdapter ]
        stripePaymentProcessor = new StripePaymentProcessor(mockFirestoreRepository, paymentIntentFactory,paymentRecordRepository,paymentRecordFactory,map)
    }

    def "charge: Should return success payment response when authentication not required"() {
        given:
        def paymentRequest = new StripePaymentRequest(
                "null",
                "PI-123-345",
                "null",
                "cust-id","ord-123")
        def paymentResponse = getPaymentResponse()

        OrderDetails orderDetails = getOrderDetails()

        SettlementAccount settlementAccount = getSettlementAmount()
        Business business = new Business("bid1", "mid1")


        PaymentIntent paymentIntent = getPaymentIntent()

        when:
        def result = stripePaymentProcessor.charge(paymentRequest)
        then:
        1 * mockFirestoreRepository.findOrderById(_ as String) >> orderDetails
        1 * mockMerchantServiceAdapter.getSettlementAccount(_ as String, _ as String, _ as SettlementAccount.AccountType) >> settlementAccount
        1 * mockMerchantServiceAdapter.getBusiness(_ as String, _ as String) >> business
        1 * paymentIntentFactory.create(_ as OrderDetails, _ as StripePaymentRequest, _ as SettlementAccount, _ as Business) >> paymentIntent

        result.status == PaymentStatus.SUCCESS
        result.intentId != null
        result.message == "Payment complete"

    }


    def "charge: Should return incomplete payment response when authentication required"() {
        given:
        def paymentRequest = new StripePaymentRequest(
                "null",
                "PI-123-345",
                "null",
                "cust-id","ord-123")
        def paymentResponse = getPaymentResponse()

        OrderDetails orderDetails = getOrderDetails()

        SettlementAccount settlementAccount = getSettlementAmount()
        Business business = new Business("bid1", "mid1")

        PaymentIntent paymentIntent = getPaymentIntent()

        paymentIntent.status = "requires_action"
        when:
        def result = stripePaymentProcessor.charge(paymentRequest)
        then:
        1 * mockFirestoreRepository.findOrderById(_ as String) >> orderDetails
        1 * mockMerchantServiceAdapter.getSettlementAccount(_ as String, _ as String, _ as SettlementAccount.AccountType) >> settlementAccount
        1 * mockMerchantServiceAdapter.getBusiness(_ as String, _ as String) >> business
        1 * paymentIntentFactory.create(_ as OrderDetails, _ as StripePaymentRequest, _ as SettlementAccount, _ as Business) >> paymentIntent

        result.status == PaymentStatus.INCOMPLETE
        result.intentId != null
        result.message == "Authentication required"

    }

    def "charge: Should return success payment response when after authentication and confirm payment"() {
        given:
        def paymentRequest = new StripePaymentRequest(
                "null",
                "PI-123-345",
                "null",
                "cust-id","ord-123")
        OrderDetails orderDetails = getOrderDetails()
        SettlementAccount settlementAccount = getSettlementAmount()
        PaymentIntent paymentIntent = Mock(PaymentIntent)
        Charge charge = new Charge()
        charge.setAmount(1700L)
        charge.setBalanceTransaction("txn-123")
        ChargeCollection chargeCollection = Mock(ChargeCollection)
        chargeCollection.setData(Collections.singletonList(charge))
        paymentIntent.setCharges(chargeCollection)

        when:
        def result = stripePaymentProcessor.charge(paymentRequest)
        then:
        0 * mockFirestoreRepository.findOrderById(_ as String) >> orderDetails
        0 * mockMerchantServiceAdapter.getSettlementAccount(_ as String, _ as String, _ as SettlementAccount.AccountType) >> settlementAccount
        1 * paymentIntentFactory.retrieve( _ as StripePaymentRequest) >> paymentIntent
        1 * paymentIntent.confirm() >> paymentIntent
        2 * paymentIntent.getCharges() >> chargeCollection
        1 * paymentIntent.getCharges().getData() >> Collections.singletonList(charge)
        1 * paymentIntent.getId() >> "PI-123-345"
        1 * paymentIntent.getClientSecret() >> "test-secret"
        1 * paymentIntent.getPaymentMethod() >> "card"
        1 * paymentIntent.getStatus() >> "succeeded"

        result.status == PaymentStatus.SUCCESS
        result.intentId == paymentRequest.intentId
        result.message == "Payment complete"
    }
}

package com.sparknow.platform.paymentservice.service

import com.sparknow.platform.paymentservice.model.StripePaymentRequest
import com.sparknow.platform.paymentservice.util.paymentProcessor.PaymentProcessor
import com.sparknow.platform.paymentservice.util.paymentProcessor.stripe.StripePaymentProcessor
import spock.lang.Specification

import static com.sparknow.platform.paymentservice.stub.PaymentStub.getPaymentResponse

class PaymentProcessorServiceTest extends Specification {
    PaymentProcessor mockStripePaymentService
    PaymentService paymentService

    def setup() {
        mockStripePaymentService = Mock(StripePaymentProcessor)
        paymentService = new PaymentService()
    }

    def "charge: Should return success payment response"() {
        given:
        def paymentRequest = new StripePaymentRequest("payment-method-id-123", null, null, "ord-123")
        def paymentResponse = getPaymentResponse()
        when:
        def result = paymentService.charge(paymentRequest)
        then:
        1 * mockStripePaymentService.charge(paymentRequest) >> paymentResponse
        result == paymentResponse
    }
}

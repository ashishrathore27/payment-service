package com.sparknow.platform.paymentservice.service

import com.sparknow.platform.paymentservice.model.SettlementAccount
import com.sparknow.platform.paymentservice.util.serviceAdapter.MerchantServiceAdapter
import com.sparknow.platform.paymentservice.util.serviceAdapter.MerchantServiceAdapter
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class MerchantServiceAdapterTest extends Specification {

    MerchantServiceAdapter settlementService

    RestTemplate restTemplate = Mock {
        def accountId = "acc_123_456"
        def platformFees = 9.9D
        SettlementAccount settlementAccount = new SettlementAccount("BI-123",SettlementAccount.AccountType.STRIPE)
        settlementAccount.id = "Id-001"
        settlementAccount.accountId = accountId
        settlementAccount.platformFees = platformFees
        settlementAccount.active = true
        settlementAccount.additionalInfo = new HashMap<String, String>()
        settlementAccount.sparknowFee = 2.15D
        getForEntity(_ as String, _ as Class<List<SettlementAccount>>, _ as Map<String, ?>) >>
                new ResponseEntity(Collections.singletonList(settlementAccount) as MerchantServiceAdapter.SettlementAccountList, HttpStatus.OK)
    }


    def setup() {
        settlementService = new MerchantServiceAdapter(restTemplate, "test/settlement")
    }

    def "findByBusinessIdAndMerchantId: Should return settlement account details for businessId and merchantd"() {
        given:
        String businessId = "BI-123"
        String merchantId = "MR-123"

        when:
        def result = settlementService.getSettlementAccount(businessId, merchantId, SettlementAccount.AccountType.STRIPE)

        then:
        result != null
        result.accountId == "acc_123_456"
        result.platformFees == 9.9D
    }
}

package com.sparknow.platform.paymentservice.resource

import com.sparknow.platform.paymentservice.model.StripePaymentRequest
import com.sparknow.platform.paymentservice.model.PaymentStatus
import com.sparknow.platform.paymentservice.service.PaymentService
import com.sparknow.platform.paymentservice.util.paymentProcessor.PaymentProcessor
import spock.lang.Specification

import static com.sparknow.platform.paymentservice.stub.PaymentStub.getPaymentResponse

class PaymentProcessorResourceTest extends Specification {
    PaymentService mockPaymentService
    PaymentResource paymentResource

    def setup() {
        mockPaymentService = Mock(PaymentProcessor)
        paymentResource = new PaymentResource(mockPaymentService)
    }

    def "charge: Should return success payment response when authentication not required"() {
        given:
        def paymentRequest = new StripePaymentRequest("Stripe",
                "payment-method-id-123",
                null,
                null,
                "ord-123")
        def paymentResponse = getPaymentResponse()
        when:
        def result = paymentResource.charge(paymentRequest)
        then:
        1 * mockPaymentService.charge(paymentRequest) >> paymentResponse
        result == paymentResponse
    }

    def "charge: Should return incomplete payment response when authentication required"() {
        given:
        def paymentRequest = new StripePaymentRequest("Stripe",
                "payment-method-id-123",
                null,
                null,
                "ord-123")
        def paymentResponse = getPaymentResponse()
        paymentResponse.status = PaymentStatus.INCOMPLETE
        paymentResponse.message = "Authentication required"
        when:
        def result = paymentResource.charge(paymentRequest)
        then:
        1 * mockPaymentService.charge(paymentRequest) >> paymentResponse
        result == paymentResponse
    }

    def "charge: Should return success payment response when request comes after authentication with PaymentIntentId"() {
        given:
        def paymentRequest = new StripePaymentRequest("Stripe",
                "payment-method-id-123",
                null,
                null,
                "ord-123")
        def paymentResponse = getPaymentResponse()
        when:
        def result = paymentResource.charge(paymentRequest)
        then:
        1 * mockPaymentService.charge(paymentRequest) >> paymentResponse
        result == paymentResponse
    }


}

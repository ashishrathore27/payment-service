package com.sparknow.platform.paymentservice.model

data class OrderDetails(
        val id: String,
        val businessId: String,
        val merchantId: String,
        val resourceId: String?,
        val currency: String?,
        val taxableAmount: Int,
        val grandTotal: Int,
        val platformType:String
)
package com.sparknow.platform.paymentservice.model

import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.stereotype.Component

@Document(collection = "Payments")
open class PaymentRecord(
        val id: String?,
        val chargedAgainst: String
) {
    var amount: Long = 0
    var transactionId: String? = null
    var status: PaymentStatus? = null

    constructor(
            paymentResponse: PaymentResponse,
            billId: String
    ) : this(
            null,
            billId
    ) {
        this.transactionId = paymentResponse.transactionId
        this.status = paymentResponse.status
        this.amount = paymentResponse.amount
    }
}


@Component
class PaymentRecordFactory {

    fun create(paymentResponse: PaymentResponse, billId: String): PaymentRecord {
        return PaymentRecord(paymentResponse, billId)
    }
}
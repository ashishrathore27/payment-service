package com.sparknow.platform.paymentservice.model


abstract class PaymentRequest(
        val paymentMode: String,
        val against: String
)

class StripePaymentRequest(
        paymentMode: String,
        val methodId: String?,
        val intentId: String?,
        val customerId: String?,
        orderId: String
) : PaymentRequest(paymentMode,orderId)



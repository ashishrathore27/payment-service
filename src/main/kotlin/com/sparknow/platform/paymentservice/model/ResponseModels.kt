package com.sparknow.platform.paymentservice.model

import com.stripe.model.Charge
import com.stripe.model.PaymentIntent

abstract class PaymentResponse(
        val amount: Long,
        val transactionId: String?
){
    var status: PaymentStatus = PaymentStatus.NONE
    var message: String = ""
    var isAdditionalActionRequired = false
}

class StripePaymentResponse(
        paymentIntent: PaymentIntent
) : PaymentResponse(
        extractTransactionAmount(paymentIntent.charges.data),
        extractTransactionId(paymentIntent.charges.data)
) {
    val intentId: String = paymentIntent.id
    val clientSecret = paymentIntent.clientSecret
}

fun extractTransactionAmount(charges: MutableList<Charge>): Long {
    return if (charges.size > 0)
        charges[0].amount.toLong()
    else
        0
}

fun extractTransactionId(charges: MutableList<Charge>): String {
    return if (charges.size > 0)
        charges[0].balanceTransaction
    else
        ""
}


enum class PaymentStatus {
    SUCCESS, FAILED, INCOMPLETE, NONE
}
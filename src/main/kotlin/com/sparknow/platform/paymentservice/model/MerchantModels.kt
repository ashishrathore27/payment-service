package com.sparknow.platform.paymentservice.model

import java.time.LocalTime

data class SettlementAccount(
        var businessId: String,
        val accountType: AccountType
) {
    var id: String? = null
    var accountId: String? = null
    var isActive: Boolean = true
    var additionalInfo = mutableMapOf<String, String>()

    var fixedFee = 35
    var sparknowFee = 4.99
    var platformFees: Double = 2.15

    enum class AccountType {
        STRIPE
    }
}

data class Business(
        var id: String,
        var merchantId: String?

) {
    var businessAttributes = BusinessAttribute()

}

class BusinessAttribute {
    var servicePlatforms = linkedSetOf<ServicePlatform>()
}

class ServicePlatform(
        var type: String,
        var platformFees: Double
)

package com.sparknow.platform.paymentservice.service

import com.sparknow.platform.paymentservice.model.PaymentRequest
import com.sparknow.platform.paymentservice.model.PaymentResponse
import com.sparknow.platform.paymentservice.util.paymentProcessor.PaymentProcessor
import org.springframework.stereotype.Service

@Service
class PaymentService(paymentProcessors: MutableList<PaymentProcessor>) {
    val paymentProcessorByName: Map<String, PaymentProcessor> = mutableMapOf()

    init {
        paymentProcessors.forEach{
            paymentProcessorByName[it.processorName]
        }
    }

    fun charge(paymentRequest: PaymentRequest): PaymentResponse {
        if(paymentProcessorByName.containsKey(paymentRequest.paymentMode)){
            return paymentProcessorByName[paymentRequest.paymentMode]?.charge(paymentRequest)!!
        }else{
            throw InvalidPaymentModeException("Invalid Payment mode " + paymentRequest.paymentMode)
        }
    }
}

class InvalidPaymentModeException(override val message: String) : Throwable()
package com.sparknow.platform.paymentservice.repository

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.cloud.firestore.Firestore
import com.sparknow.platform.paymentservice.model.OrderDetails
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository
import java.util.*


@Repository
class FirestoreRepository(
        private val db: Firestore,
        private val objectMapper: ObjectMapper,
        @Value("\${firestore.order.collection}") private val collection: String
) {

    fun findOrderById(id: String): OrderDetails {
        val orderCollection = db.collection(collection)

        return Optional.ofNullable(orderCollection.document(id))
                .map { it.get() }
                .map { it.get() }
                .map { objectMapper.convertValue(it.data, OrderDetails::class.java) }
                .orElseThrow { NoSuchElementException("No order found for orderId : $id")  }
    }
}

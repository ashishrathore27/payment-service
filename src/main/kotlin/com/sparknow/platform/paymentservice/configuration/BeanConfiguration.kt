package com.sparknow.platform.paymentservice.configuration

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.Firestore
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.cloud.FirestoreClient
import com.sparknow.platform.paymentservice.util.serviceAdapter.ServiceAdapter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class BeanConfiguration(
        @Value("\${firestore.project.id}") private val projectId: String
) {
    @Autowired
    var serviceAdapters: List<ServiceAdapter> = mutableListOf()

    @Bean
    fun initializeFireStore(): Firestore {
        val credentials = GoogleCredentials.getApplicationDefault()
        val options = FirebaseOptions.Builder()
                .setCredentials(credentials)
                .setProjectId(projectId)
                .build()
        FirebaseApp.initializeApp(options)
        return FirestoreClient.getFirestore()
    }

    @Bean
    fun restTemplate(): RestTemplate {
        return RestTemplateBuilder().build()
    }

    @Bean
    fun objectMapper(): ObjectMapper {
        return ObjectMapper()
                .registerKotlinModule()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }


    @Bean
    fun serviceAdaptersByName(): Map<String, ServiceAdapter>{
        val serviceAdaptersByName = mutableMapOf<String, ServiceAdapter>()
        serviceAdapters.forEach { adapter ->
            serviceAdaptersByName[adapter.name] = adapter
        }

        return serviceAdaptersByName
    }
}
package com.sparknow.platform.paymentservice.resource

import com.sparknow.platform.paymentservice.model.PaymentRequest
import com.sparknow.platform.paymentservice.model.PaymentResponse
import com.sparknow.platform.paymentservice.service.PaymentService
import org.springframework.web.bind.annotation.*


@CrossOrigin(origins = ["*"])
@RestController
@RequestMapping("charge")
class PaymentResource(private val paymentService: PaymentService) {

    @PostMapping
    fun charge(@RequestBody paymentRequest: PaymentRequest): PaymentResponse {
        return paymentService.charge(paymentRequest)
    }
}



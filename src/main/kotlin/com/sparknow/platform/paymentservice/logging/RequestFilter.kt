package com.sparknow.platform.paymentservice.logging

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class RequestFilter : OncePerRequestFilter() {

    val log: Logger = LoggerFactory.getLogger(RequestFilter::class.java)

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val successfulRegistration = registerRequestInMDC(request)
        filterChain.doFilter(request, response)
        if (successfulRegistration) {
            deregisterRequestInMDC()
        }
    }


    private fun registerRequestInMDC(request: HttpServletRequest): Boolean {
        MDC.put("URI", request.requestURI)
        MDC.put("httpMethod", request.method)
        MDC.put("sessionId", request.getHeader("sessionId"))
        MDC.put("orderId", request.getHeader("orderId"))

        return true
    }

    private fun deregisterRequestInMDC() {
        MDC.remove("URI")
        MDC.remove("httpMethod")
        MDC.remove("sessionId")
        MDC.remove("orderId")
    }

}
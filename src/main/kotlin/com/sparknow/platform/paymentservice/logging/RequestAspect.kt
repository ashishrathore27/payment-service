package com.sparknow.platform.paymentservice.logging

import net.logstash.logback.argument.StructuredArguments.f
import net.logstash.logback.argument.StructuredArguments.kv
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.util.StopWatch


@Aspect
@Component
class RequestAspect {

    @Pointcut("within(@org.springframework.stereotype.Repository *)" +
            " || within(@org.springframework.stereotype.Service *)" +
            " || within(@org.springframework.web.bind.annotation.RestController *)")
    fun springBeanPointcut() {
    }


    @Around("springBeanPointcut()")
    fun logExecutionTime(joinPoint: ProceedingJoinPoint): Any? {
        val logger = LoggerFactory.getLogger(joinPoint.target.javaClass)
        val stopWatch = StopWatch()
        stopWatch.start()
        val signature = joinPoint.signature.toShortString()
        try {
            logger.info("Beginning Execution: $signature", f(joinPoint.args))
            return joinPoint.proceed()
        } catch (throwable: Throwable) {
            logger.error("Exception in execution: $signature", f(throwable.cause))
            throw throwable
        } finally {
            stopWatch.stop()
            logger.info("Exuction End: $signature", kv("execution-time", stopWatch.totalTimeMillis))
        }
    }
}
package com.sparknow.platform.paymentservice.util.serviceAdapter

import org.springframework.web.client.RestTemplate

abstract class ServiceAdapter(val name: String, val restTemplate: RestTemplate, val serviceBaseUrl: String)
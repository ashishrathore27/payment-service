package com.sparknow.platform.paymentservice.util.paymentProcessor

import com.sparknow.platform.paymentservice.model.PaymentRecord
import com.sparknow.platform.paymentservice.model.PaymentRequest
import com.sparknow.platform.paymentservice.model.PaymentResponse
import com.sparknow.platform.paymentservice.util.serviceAdapter.ServiceAdapter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.repository.MongoRepository

interface PaymentRecordRepository: MongoRepository<PaymentRecord, String>


abstract class PaymentProcessor(
        val processorName: String,
        val serviceAdapters : Map<String, ServiceAdapter>
) {

    abstract fun charge(paymentRequest: PaymentRequest): PaymentResponse
}
package com.sparknow.platform.paymentservice.util.paymentProcessor.stripe

import com.sparknow.platform.paymentservice.model.SettlementAccount.AccountType.STRIPE
import com.sparknow.platform.paymentservice.repository.FirestoreRepository
import com.sparknow.platform.paymentservice.util.serviceAdapter.MerchantServiceAdapter
import com.sparknow.platform.paymentservice.util.paymentProcessor.PaymentProcessor
import com.sparknow.platform.paymentservice.util.paymentProcessor.PaymentRecordRepository
import com.sparknow.platform.paymentservice.util.serviceAdapter.ServiceAdapter
import com.sparknow.platform.paymentservice.model.*
import com.stripe.model.PaymentIntent
import org.springframework.stereotype.Component


@Component
class StripePaymentProcessor(
        private val firestoreRepository: FirestoreRepository,
        private val paymentIntentFactory: PaymentIntentFactory,
        private val paymentRecordRepository: PaymentRecordRepository,
        private val paymentRecordFactory: PaymentRecordFactory,
        serviceAdapters : Map<String, ServiceAdapter>
) : PaymentProcessor("Stripe",serviceAdapters) {

    val merchantServiceAdapter = serviceAdapters["Merchant"] as MerchantServiceAdapter


    override fun charge(paymentRequest: PaymentRequest): StripePaymentResponse {
        if(paymentRequest is StripePaymentRequest) {

            val stripePaymentRequest: StripePaymentRequest = paymentRequest

            return stripePaymentRequest.intentId?.let {
                updateResponse(confirmPayment(stripePaymentRequest),paymentRequest)
            } ?: updateResponse(initiatePayment(stripePaymentRequest),paymentRequest)
        }else{

            throw InvalidPaymentRequestForSelectedMode("Invalid payment request for payment mode Stripe")
        }
    }

    private fun initiatePayment(stripePaymentRequest: StripePaymentRequest): PaymentIntent {
        val orderDetails: OrderDetails = firestoreRepository
                .findOrderById(stripePaymentRequest.against)
        val settlementAccount = merchantServiceAdapter
                .getSettlementAccount(orderDetails.businessId, orderDetails.merchantId, STRIPE)
        val business = merchantServiceAdapter
                .getBusiness(orderDetails.businessId, orderDetails.merchantId)

        return paymentIntentFactory
                .create(orderDetails, stripePaymentRequest, settlementAccount, business)
    }

    private fun confirmPayment(stripePaymentRequest: StripePaymentRequest): PaymentIntent {
        val intent: PaymentIntent = paymentIntentFactory.retrieve(stripePaymentRequest)
        return intent.confirm()
    }

    private fun updateResponse(intent: PaymentIntent, paymentRequest: PaymentRequest): StripePaymentResponse {
        val paymentResponse = StripePaymentResponse(intent)
        when (intent.status) {
            "requires_action", "requires_source_action" -> {
                paymentResponse.isAdditionalActionRequired = true
                paymentResponse.status = PaymentStatus.INCOMPLETE
                paymentResponse.message = "Authentication required"
            }
            "requires_payment_method", "requires_source" -> {
                paymentResponse.status = PaymentStatus.FAILED
                paymentResponse.message = "Your card was denied, please provide a new payment method";
                paymentRecordRepository.save(paymentRecordFactory.create(paymentResponse,paymentRequest.against))
            }
            "succeeded" -> {
                paymentResponse.status = PaymentStatus.SUCCESS
                paymentResponse.message = "Payment complete"
                paymentRecordRepository.save(paymentRecordFactory.create(paymentResponse,paymentRequest.against))
            }
            else -> paymentResponse.message = "Unrecognized status"
        }
        return paymentResponse
    }


}

class InvalidPaymentRequestForSelectedMode(override val message: String) : Throwable()
package com.sparknow.platform.paymentservice.util.paymentProcessor.stripe

import com.sparknow.platform.paymentservice.model.*
import com.stripe.Stripe
import com.stripe.model.PaymentIntent
import com.stripe.param.PaymentIntentCreateParams
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import kotlin.math.roundToLong

@Component
class PaymentIntentFactory(
        @Value("\${stripe.api.key}") private val apiKey: String
) {

    fun create(
            orderDetails: OrderDetails,
            stripePaymentRequest: StripePaymentRequest,
            settlementAccount: SettlementAccount,
            business: Business
    ): PaymentIntent {

        val platform: ServicePlatform? = business.businessAttributes.servicePlatforms.find { it.type == orderDetails.platformType }

        val transferAmount = calculateTransferAmount(
                orderDetails,
                platform?.platformFees ?: 0.0,
                settlementAccount
        )

        val params = PaymentIntentCreateParams.builder()
                .setCurrency(orderDetails.currency)
                .setAmount(orderDetails.grandTotal.toLong())
                .setPaymentMethod(stripePaymentRequest.methodId)
                .setConfirm(true)
                .setConfirmationMethod(PaymentIntentCreateParams.ConfirmationMethod.MANUAL)
                .setErrorOnRequiresAction(false)
                .setTransferData(PaymentIntentCreateParams.TransferData.builder()
                        .setAmount(transferAmount)
                        .setDestination(settlementAccount.accountId)
                        .build())
                .putMetadata("orderId", orderDetails.id)
                .putMetadata("businessId", orderDetails.businessId)
                .build()

        return PaymentIntent.create(params)
    }

    fun retrieve(stripePaymentRequest: StripePaymentRequest): PaymentIntent {
        return PaymentIntent.retrieve(stripePaymentRequest.intentId)
    }

    protected fun calculateTransferAmount(
            orderDetails: OrderDetails,
            platformFees: Double,
            settlementAccount: SettlementAccount
    ): Long {
        val totalAmount = orderDetails.grandTotal

        val taxableAmount = orderDetails.taxableAmount

        val transactionFee = (taxableAmount * platformFees) / 100
        val sparknowFee = ((totalAmount - transactionFee) * (settlementAccount.sparknowFee / 100)) + settlementAccount.fixedFee
        return (totalAmount - sparknowFee - transactionFee).roundToLong()
    }

    init {
        Stripe.apiKey = apiKey
    }
}
package com.sparknow.platform.paymentservice.util.serviceAdapter

import com.sparknow.platform.paymentservice.model.Business
import com.sparknow.platform.paymentservice.model.SettlementAccount
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.util.*
import kotlin.NoSuchElementException
import kotlin.collections.LinkedHashMap

@Service(value = "MerchantAdapter")
class MerchantServiceAdapter(
        restTemplate: RestTemplate,
        @Value("\${merchant.service.url}") merchantServiceBaseUrl: String
): ServiceAdapter(
        "MerchantService",
        restTemplate,
        merchantServiceBaseUrl
) {

    class SettlementAccountList : MutableList<SettlementAccount> by ArrayList()

    fun getSettlementAccount(
            businessId: String,
            merchantId: String,
            accountType: SettlementAccount.AccountType
    ): SettlementAccount {
        val uriVariable = LinkedHashMap<String, String>()
        uriVariable["merchantId"] = merchantId
        uriVariable["businessId"] = businessId

        val settlementAccount = restTemplate
                .getForEntity("$serviceBaseUrl/settlementaccounts", SettlementAccountList::class.java, uriVariable)

        return Optional.ofNullable(settlementAccount.body)
                .map {
                    it.stream()
                            .filter { it.accountType == accountType }
                            .findFirst()
                            .orElse(null)
                }
                .orElseThrow {
                    NoSuchElementException("No settlement account for businessId : $businessId")
                }
    }


    fun getBusiness(
            businessId: String,
            merchantId: String
    ): Business {
        val uriVariable = LinkedHashMap<String, String>()
        uriVariable["merchantId"] = merchantId
        uriVariable["businessId"] = businessId

        val business = restTemplate
                .getForEntity(serviceBaseUrl, Business::class.java, uriVariable)

        return Optional.ofNullable(business.body)
                .orElseThrow {
                    BusinessNotFoundException("No business account found for businessId : $businessId")
                }
    }
}

class BusinessNotFoundException(override val message: String) : Throwable()




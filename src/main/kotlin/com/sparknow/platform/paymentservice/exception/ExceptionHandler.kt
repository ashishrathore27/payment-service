package com.sparknow.platform.paymentservice.exception

import com.stripe.exception.*
import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandler {

    private val logger = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(CardException::class)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    fun handleCardException(exception: CardException): ResponseEntity<Any> {
        logger.error(exception.message, exception)
        return ResponseEntity(mapOf("message" to exception.message), HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @ExceptionHandler(RateLimitException::class)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    fun handleRateLimitException(exception: RateLimitException): ResponseEntity<Any> {
        logger.error(exception.message, exception)
        return ResponseEntity(mapOf("message" to exception.message), HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @ExceptionHandler(InvalidRequestException::class)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    fun handleInvalidRequestException(exception: InvalidRequestException): ResponseEntity<Any> {
        logger.error(exception.message, exception)
        return ResponseEntity(mapOf("message" to exception.message), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(AuthenticationException::class)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    fun handleAuthenticationException(exception: AuthenticationException): ResponseEntity<Any> {
        logger.error(exception.message, exception)
        return ResponseEntity(mapOf("message" to exception.message), HttpStatus.UNAUTHORIZED)
    }


    @ExceptionHandler(StripeException::class)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    fun handleStripeException(exception: StripeException): ResponseEntity<Any> {
        logger.error(exception.message, exception)
        return ResponseEntity(mapOf("message" to exception.message), HttpStatus.EXPECTATION_FAILED)
    }

}